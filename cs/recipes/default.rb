#
# Cookbook:: cs
# Recipe:: default
#
# Copyright:: 2017, Suresh Skanda, All Rights Reserved.

cs_install_dir=node[:cs][:install_dir]

execute "cs process stop" do
  user "root"
  group "root"
  cwd cs_install_dir
  action :run
  command "fuser -k 8888/tcp"
end

execute "delete" do
  user "root"
  group "root"
  cwd cs_install_dir
  action :run
  command "rm -rf config-server.jar"
end

remote_file "/home/dofedex/chefdata/config-server-#{node[:cs][:version]}.jar" do
  source "http://fedexnexus.centralus.cloudapp.azure.com:8081/repository/configserver-releases/com/fedexspringio/skanda/config-server/#{node[:cs][:version]}/config-server-#{node[:cs][:version]}.jar"
  owner "root"
  group "root"
  mode '0755'
end

execute 'cs mv' do
  user  'root'
  group "root"
  cwd cs_install_dir
  action :run
  command "mv config-server-#{node[:cs][:version]}.jar config-server.jar"
end

execute 'cs run' do
  user  'root'
  group "root"
  cwd cs_install_dir
  action :run
  command "echo 'java -jar config-server.jar' | at now + 1 minutes"
end
